class LeavingPopup {

  leavingPopupElement;
  leavingPopup;

  constructor(selector = "#leavingPopup", delay = 5000) {

    this.leavingPopupElement = document.querySelector(selector);
    this.leavingPopup = new bootstrap.Modal(this.leavingPopupElement);

    setTimeout(() => {

      this.maskPhone();

      this.leavingPopup.toggle();
      // document.querySelector('html')
      //         .addEventListener("mouseleave", this.confirmExit);

    }, delay);
  }

  maskPhone = () => {

    let phoneElements;

    phoneElements = this.leavingPopupElement.querySelectorAll('[type="phone"]');
    phoneElements = Array.from(phoneElements);

    phoneElements.forEach(phone => {

      phone.onkeypress = function() { PhoneMaskUtils.mask( this )};
    })
  }

  confirmExit = (event) => {

    const cookieName = `pop_shown`;

    if( ! Utils.hasCookie(cookieName) ) {

      Utils.setCookie(cookieName, true);
    
      return this.leavingPopup.toggle();
    }

    return `Modal has been shown already`
  }
}

class Utils {

  static setCookie = (name, value) => {

    document.cookie = `${name} = ${value}`;
  }
  
  static getCookieValueFor = (name) =>  {
  
    name = name + "=";

    const decodedCookie = decodeURIComponent(document.cookie);
    const ca = decodedCookie.split(';');
  
    for(let index = 0, max = ca.length; index < max; index++) {
  
      let cookie = ca[index];
  
      while (cookie.charAt(0) == ' ') {
        cookie = cookie.substring(1);
      };
  
      if (cookie.indexOf(name) == 0) return cookie.substring(name.length, cookie.length);
    }
  
    return false;
  }
  
  static hasCookie = (name) =>  {
  
    const cookie = this.getCookieValueFor(name);
  
    if (cookie) return true;
  
    return false
  }
}

/* this is for brazilian users, only */
class PhoneMaskUtils {

  static mask(obj, callback = this.mtel){

    setTimeout(() => { obj.value = callback(obj.value) }, 1);
  }

  static mtel(phone){

    phone = phone.replace(/\D/g,"");
    phone = phone.replace(/^(\d{2})(\d)/g,"($1) $2");
    phone = phone.replace(/(\d)(\d{4})$/,"$1-$2");

    return phone;
  }
}
